# LabSO Meta Research

#### Content related to the study conducted by Inno³  (July-September 2021) requested by the French Committee for Open Science (CoSO) in order to pave the way for an Open Science Lab (LabSO).

The content of this GitLab project reflects the quantitative phase of the study, which provided insights for the qualitative phase of the study, the resulting analyses of which were presented at  the Open Science Lab pre-launch seminar by Célya Gruson-Daniel and Maya Anderson-Gonzalez on September 27th, 2021.

## Here is what you will find on this GitLab project : 


1️⃣ `Hyphe` folder containing :

- a .txt file with the list of 78 URLs which were imported to the web-crawler [Hyphe](http://hyphe.medialab.sciences-po.fr/)
- a .gexf file extracted from Hyphe for visual processing with [Gephi](https://gephi.org/).

2️⃣ `Open Knowledge Maps` folder containing screenshots of bibliometric network visualisations made with [Open Knowledge Maps](https://openknowledgemaps.org/) (2019): A Visual Interface to the World's Scientific Knowledge. 

3️⃣ `Viz_Voyant-tools ` folder containing a sub-folder with screenshots of word clouds which were made with [Voyant Tools](https://voyant-tools.org/) and a .md file with the terms used to generate the clouds. 

4️⃣ `source_SigmaJS` folder containing the data used for making a digital map with [SigmaJS](https://gitlab.com/inno3/labso-meta-research-public/-/tree/main/source_SigmaJS).

Enjoy exploring! 🔎


#### Contenu relatif à l'étude menée par inno³ pour accompagner le projet de structuration d'un Laboratoire de la science ouverte (LabSO) par le Comité pour la Science Ouverte (CoSO).

Le contenu de ce projet GitLab représente le volet quantitaif de l'étude et accompagne le travail qualitatif d'entretien et d'analyse dont les résutlats ont été présentés au séminaire de préfiguration du Lab de la science ouverte par Célya Gruson-Daniel et Maya Anderson-Gonzalez le 27 septembre 2021.

## Vous trouverez sur ce projet Gitlab :


:one: un dossier `Hyphe` contenant:

- un fichier .txt avec la liste des 78 URL importées dans l'outil [Hyphe](http://hyphe.medialab.sciences-po.fr/)

- un fichier .gexf extrait de l'outil Hyphe et possible de traiter sur [Gephi](https://gephi.org/).

:two: un dossier `Open Knowledge Maps` avec des captures d'écran des visualisations des réseaux bibliométriques effectués avec [Open Knowledge Maps](https://openknowledgemaps.org/). 

:three: un dossier `Viz_Voyant-tools` avec des captures d'écran de nuages de mots créés avec [Voyant Tools](https://voyant-tools.org/) et un fichier .md avec les termes utilisés pour les générer.

:four: un dossier `source_SigmaJS` avec les données qui vous permettent de voir la carte numérique visualisée à l'aide de l'outil [Sigma](https://gitlab.com/inno3/labso-meta-research-public/-/tree/main/source_SigmaJS).

Bonne exploration!  :mag_right:
