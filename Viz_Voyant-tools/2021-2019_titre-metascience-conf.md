
Metascience 2021 

Open Q&A
After excellence: Why it’s time to diversify the criteria we use to fund and evaluate research

Lisette Jong, Cameron Neylon, Laura Rovelli, Zena Sharman
Moderator: Stephen Pinfield

Open Q&A
Bolstering accountability and self-skepticism within the metascience movement

Rachel Ankeny, Hilda Bastian, Bart Penders, Thomas Schillemans
Moderator: Simine Vazire

Symposia
Building a research culture for replicability and reproducibility in the social sciences

Jade Benjamin-Chung, Fernando Hoces de la Guardia, Nick Fox, Olivia Miske, Ana Trisovic
Moderator: Aleksandar Bogdanoski

Open Q&A
Covering science: Assessing and representing uncertainty, credibility, and reform

Christie Aschwanden, Monya Baker, Richard Harris
Moderator: Kathleen Hall Jamieson

Theory to Action
Crowdfight: Fostering collaboration by making visible the invisible help

Alfonso Pérez Escudero, María Hernández-Sánchez, Ana Morán, Alberto Pascual-García

Debate
Debate: Are QRPs misconduct/an ethics violation?

Dorothy Bishop, Paul Glasziou, Jane Hutton, Daniel Lakens

Debate
Debate: Should peer review occur before or after publication?

Liam Kofi Bright, Remco Heesen, Daniela Saderi, Emily Sena

Symposia
Do replications make a difference?

Tom Coupe, Rose O’Dea, W. Robert Reed

Discussion
Empowering early career researchers to improve science

Humberto Debat, Nafisa Jadavji, Gary McDowell, Kleber Neves, Tracey Weissgerber
Moderator: Brianne Kent

Discussion
Engaging the community in research reproduction

Stephen Eglen, Sergey Frolov, Anna Krystalli, Etienne Roesch
Moderator: Cassio Sozinho Amorim

Open Q&A
Error detection and correction

Elisabeth Bik, James Heathers, Stephanie Lee
Moderator: Adam Marcus

Symposia
Evaluating (and addressing) common shortcomings in peer-reviewed research

Hilde Augusteijn, Marjan Bakker, Robbie C.M. van Aert
Moderator: Richard A. Klein

Discussion
Forecasting scientific outcomes

Anna Dreber, Fiona Fidler, Sarah Rajtmajer, Eva Vivalt
Moderator: Timothy Errington

Symposia
How biology differs from other sciences, and why this matters for Metascience

John Dupre, Steven Goodman, Nicole C. Nelson, Robert Weinberg
Moderators: David Peterson, Pamela Reinagel

Discussion
How metascience research fits into broader worldviews: Perspectives from funders

Susan Fitzpatrick, Nicholas Gibson, Arthur Lupia, Dawid Potgieter
Moderator: Erin McKiernan

Symposia
Incorporating open science into evidence-based practice: The TRUST Initiative

Sean Grant, Sina Kianersi, Kevin Naaman, Lauren Supplee
Moderator: Evan Mayo-Wilson

Symposia
Innovating peer review, reconfiguring scholarly communication

John Inglis, Wolfgang Kaltenbrunner, Sowmya Swaminathan, Ludo Waltman, Victoria Yan
Moderator: Stephen Pinfield

Symposia
Lessons for the lab: How metaresearch is shaping basic biomedical research

Alexandra Bannach-Brown, Manoj Lalu, Takuji Usui, Tracey Weissgerber
Moderator: Malcolm MacLeod

Discussion
Lessons from the “Big Team Science” movement

Drew Altschul, Nicholas A. Coles, Kiley Hamlin, Tim Parker, Lauren Sullivan
Moderator: Patrick S. Forscher

Lightning Talks
Lightning Talk Session A

Till Bruckner, Jessica Butler, Ben Meghreblian, Maia Salholz-Hillel, Ye Sun, Marta Topor

Lightning Talks
Lightning Talk Session B

Céline Heinl, Robert Schulz, Alexa Tullet

Lightning Talks
Lightning Talk Session C

Cassio Sozinho Amorim, Sabine Bischoff, Cozette Comer and Nathaniel Porter, Daniel D. Drevon, Gabriela Gore-Gorszewska, Thomas Hostler, Patricia Martinkova, Dragan Okanovic

Lightning Talks
Lightning Talk Session D

Alex O. Holcombe, Hassan Khan, Alyssa Mikytuck, W. Robert Reed, Cooper Smout

Lightning Talks
Lightning Talk Session E

Nick Fox, Keven Joyal-Desmarais, Pranujan Pathmendra, Dominique Roche, Simon Schwab, Joshua D. Wallach

Lightning Talks
Lightning Talk Session F

Wes Bonifay, Samuel C. Fletcher, Nicholas Otis, David Reinstein, Bianca Trovò, Kaitlin Stack Whitney

Lightning Talks
Lightning Talk Session G

Sakshi Ghai, Nicole C. Nelson, Jürgen Schneider, Priscilla Van Even

Lightning Talks
Lightning Talk Session H

Christopher L. Aberson, Shaakya Anand-Vembar, Yuqing Cai

Lightning Talks
Lightning Talk Session I

Peter Andre, Alfonso Pérez Escudero, Alexander Herwix, Meng Liu, Rubén López-Nicolás, Alejandra Manco, James Andrew Smith

Lightning Talks
Lightning Talk Session J

Theodor Cucu, Ricky Jeffrey, David Lang, David Mehler, Alejandra Recio-Saucedo, Robert Ross, Jason Williams

Discussion
Many perspectives on many analysts research: A moderated panel discussion

Balázs Aczél, Rotem Botvinik-Nezer, Wilson Cyrus-Lai, Eric Uhlmann
Moderator: Nate Breznau

Discussion
Metascience: Disrupting the status quo or perpetuating inequities

Dani S. Bassett, Diego Kozslowski
Moderator: Cassidy Sugimoto

Discussion
Open science, but correctly! Interacting with new forms of openness in research and society

Nate Breznau, Tamara Heck, Sylvia Kullmann, Jana Lasser
Moderator: Katja Mayer

Symposia
P-hacking revisited: Comprehensive modeling of the process of conducting scientific research

Amir Masoud Abdol, Anton Olsson-Collentine, Eric-Jan Wagenmakers, Jelte Wicherts
Moderator: Esther Maassen

Discussion
Practical and responsive science in the age of COVID-19

Elliot Berkman, Bronwyn MacInnis, Adam Russell
Moderators: Kristin Brethel-Haurwitz, Luke Stoeckel

Theory to Action
Preregistration in the social sciences: Empirical evidence of its effectiveness

Sarahanne Field, George Ofosu, Marcel van Assen
Moderator: Olmo van den Akker

Discussion
Promoting open science and transparency across sub-disciplines in psychology

Lillian Eby, Colin Wayne Leach, Fred Oswald, Keith O. Yeates
Moderators: Peggy Christidis, Rosemarie Sokol-Chang

Symposia
Psychology’s crisis of confidence: Measurement edition

Jessica K. Flake, Eiko Fried, Esther Maassen, Andrea Helena Stoevenbelt

Symposia
Reasonable, questionable or inexcusable? Do we need to do more to protect academic publishing against editorial misbehaviour?

Ginny Barbour, Daniel Hamilton, Rink Hoekstra, Simine Vazire

Theory to Action
Registered Reports

Chris Chambers, Stavroula Kousta, Anne Scheel, Courtney Soderberg
Moderator: David Mellor

Open Q&A
Resetting research: How can metascience support post-pandemic transformations in research cultures, funding and evaluation?

Matthias Egger, Chonnettia Jones, Dorsamy (Gansen) Pillay, Karen Salt
Moderator: James Wilsdon

Discussion
Science to policy: What do we know about the science that becomes law?

Sean Grant, Anna Harvey, Rob MacCoun, Kathy Zeiler
Moderator: Philip Cohen

Symposia
ScreenIT: Can we use automated screening tools to improve reporting in scientific papers?

Anita Bandrowski, Peter Eckmann, Colby Vorland, Tracey Weissgerber
Moderator: Halil Kilicoglu

Symposia
Structural racism in biomedicine

Kenneth Gibbs, Aletha Maybank
Moderator: Carole J. Lee

Symposia
The role of tools in accelerating scientific discovery

Jungwon Byun, Joel Chan, Karola Kirsanow, David Lang, Ben Reinhardt

Discussion
Unintended consequences of science reforms

Denny Borsboom, Sophia Crüwell, Anna Dreber, Karthik Panchanathan
Moderators: Leo Tiokhin, Noah van Dongen

Symposia
Upending white supremacy in science

Nicole T. Buchanan, Marisol Perez, Mitchell J. Prinstein, Idia Thurston

Discussion
Uptake of preregistration in different research fields – an exchange of experiences

Marjan Bakker, Nicholas DeVito, Céline Heinl
Moderator: Ulf Tölch

Open Q&A
What is metascience? Part 1: Methods, disciplines and purposes

Sarah de Rijcke, Charlie Ebersole, Santo Fortunato, Aileen Fyfe, Cassidy Sugimoto
Moderator: James Wilsdon

Open Q&A
What is metascience? Part 2: Institutions, networks and future priorities

Carole Lee, Nicole C. Nelson, Brian Nosek, Simine Vazire, Dashun Wang
Moderator: Fiona Fidler

Metascience 2019 



        Carl Bergstrom: The inherent inefficiency of grant proposal competitions and the possible benefits of lotteries in allocating research funding
        Dorothy Bishop: The psychology of scientists: The role of cognitive biases in sustaining bad science
        Annette N. Brown: Is replication research the study of research or of researchers?
        Tim Errington: Barriers to conducting replications – challenges or opportunities?
        James Evans: The social limits of scientific certainty
        Daniele Fanelli: Low reproducibility as divergent information: A K-theory analysis of reproducibility studies
        Fiona Fidler: Misinterpretations of evidence, and worse misinter- pretations of evidence
        Jacob Foster: Made to Know: Science as the Social Production of Knowledge (and its Limits)
        Andrew Gelman: Embracing Variation and Accepting Uncertainty: Implications for Science and Metascience
        Steven Goodman: Statistical methods as social technologies versus analytic tools: Implications for metascience and research reform
        Zoltán Kekecs: How to produce credible research on anything
        Carole Lee: Gender-based homophily in collaborations across a heterogeneous scholarly landscape
        Edward Miguel: Innovations in Pre-registration in Economics
        Staša Milojević: The Changing Landscape of Knowledge Production
        Michèle Nuijten: Checking Robustness in 4 Steps
        Cailin O’Connor: Scientific Polarization
        Adam Russell: Fomenting (Reproducible) Revolutions: DARPA, Re- plication, and High-Risk, High-Payoff Research
        Marta Sales-Pardo: Collaboration patterns in science
        Melissa Schilling: Where do breakthrough ideas come from?
        Jonathan Schooler: How replicable can psychological science be?: A highly powered multi-site investigation of the robustness of newly discovered findings
        Dean Keith Simonton: Scientific Creativity: Discovery and Invention as Combinatorial
        Roberta Sinatra: Quantifying the evolution of scientific careers
        Paula Stephan: Practices and Attitudes Regarding Risky Research
        Simine Vazire: Towards a More Self-Correcting Science
        Bernhard Voekl: Biological variation is more than random noise
        Jan Walleczek: Counterfactual Meta-Experimentation and the Limits of Science: 100 Years of Parapsychology as a Control Group
        Shirley Wang: What does replicable ‘real world’ evidence from ‘real world’ data look like?
        Jevin West: Echo Chambers in Science?
        Yang Yang: The Replicability of Scientific Findings Using Human and Machine Intelligence


