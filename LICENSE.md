- Unless otherwise specified, the content created by inno³ (text and images) published on this site is subject to the terms of the [CC-BY 4.0 licence](https://creativecommons.org/licenses/by/4.0/). The data and script are under the [open licence 2.0 Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf)
- For Sigma.Js, we use the export option for Gephi. For Gephi, the main source code is distributed under the dual license CDDL 1.0 and GNU General Public License v3. 

